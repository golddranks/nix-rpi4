{ config, pkgs, lib, ... }:

let secrets = import ./secrets.nix;

in

{
	imports = [
		<nixpkgs/nixos/modules/installer/cd-dvd/sd-image-raspberrypi4.nix>
		./hardware-configuration.nix
	];

	sdImage.compressImage = false;
	sdImage.populateFirmwareCommands = ''
		${config.system.build.installBootLoader} ${config.system.build.toplevel} -d ./
	'';
	sdImage.populateRootCommands = ''
		mkdir -p ./files/var/empty
	'';

	fileSystems = lib.mkForce {
		"/boot" = {
			device = "/dev/disk/by-label/FIRMWARE";
			fsType = "vfat";
			# we NEED this mounted
		};
		"/" = {
			device = "/dev/disk/by-label/NIXOS_SD";
			fsType = "ext4";
		};
	};

	services.timesyncd.enable = true;

	networking.enableIPv6 = false;
	networking.wireless = {
		enable = true;
		interfaces = [ "wlan0" ];
		networks = secrets.wirelessNetworks;
	};
	networking.nameservers = [ "1.1.1.1" ];
	networking.defaultGateway = {
		address = secrets.networkAddr.gateway;
		interface = "wlan0";
	};
	networking.interfaces.wlan0.ipv4 = {
		addresses = [{
			address = secrets.networkAddr.address;
			prefixLength = 24;
		}];
	};

	services.sshd.enable = true;
	users.users.root.initialHashedPassword = secrets.defaultPassword;
	
	systemd.services.sshd.wantedBy = lib.mkOverride 10 [ 
		"default.target"
	];

	systemd.services.wpa_supplicant.wantedBy = lib.mkOverride 10 [ 
		"default.target"
	];
}
