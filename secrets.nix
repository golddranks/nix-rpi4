{
	wirelessNetworks = {
		"NAME" = {
			psk = "PASSWORD";
		};
	};

	networkAddr = {
		address = "192.168.1.169";
		gateway = "192.168.1.254";
	};

	sshKeys = [];

	# password: "password"
	defaultPassword = 
		"$6$kRpI1h4RlNelJ/u$lYJmNiQ03B.Q7jfdOvWLN6jv9aPf53geVa9RHsQ1t5WWDqOgjCFgh1or.03YXT1JGI7ySUXLyR1Eyscqq5vXZ/";
}
