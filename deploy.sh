#!/usr/bin/env bash
set -e

(( $# == 1 )) || {
	echo "usage: $0 switch"
	exit
}

# Grab the Raspberry Pi address
addr=$(nix eval --raw -- "(import ./secrets.nix).networkAddr.address")

echo "Moving Nix configurations to remote..."

# Move the current folder over and sync
rsync -rz --info=progress2 -e "ssh $SSH_ARGS"    \
	--include "secrets.nix"                      \
	--exclude-from ".gitignore" --exclude ".git" \
	. "root@$addr:/tmp/nix"

# Start building on the remote host
ssh $SSH_ARGS "root@$addr" -- nixos-rebuild $1 \
	-I nixos-config=/tmp/nix/configuration.nix

